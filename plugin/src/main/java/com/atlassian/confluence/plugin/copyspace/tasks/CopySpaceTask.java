package com.atlassian.confluence.plugin.copyspace.tasks;

import com.atlassian.confluence.plugin.copyspace.CopySpaceManager;
import com.atlassian.confluence.plugin.copyspace.CopySpaceOptions;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.longrunning.ConfluenceAbstractLongRunningTask;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public class CopySpaceTask extends ConfluenceAbstractLongRunningTask {

    protected final ConfluenceUser user;
    protected TransactionTemplate transactionTemplate;
    private Space originalSpace;
    private String newKey;
    private String newName;
    private CopySpaceOptions options;
    private CopySpaceManager copySpaceManager;

    public CopySpaceTask(Space originalSpace, String newKey, String newName,
            CopySpaceOptions options, ConfluenceUser user, CopySpaceManager copySpaceManager,
            TransactionTemplate transactionTemplate) {
        this.originalSpace = originalSpace;
        this.newKey = newKey;
        this.newName = newName;
        this.options = options;
        this.user = user;
        this.copySpaceManager = copySpaceManager;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public String getName() {
        return "Copy Space";
    }

    @Override
    protected void runInternal() {
        AuthenticatedUserThreadLocal.set(user);
        CopySpaceTaskTransactionCallback tc = new CopySpaceTaskTransactionCallback();
        transactionTemplate.execute(tc);
    }

    private class CopySpaceTaskTransactionCallback implements TransactionCallback<Space> {

        @Override
        public Space doInTransaction() {
          try {
              progress.setStatus("Start to copy space...");
              Space newSpace;
              ProgressUpdater progressUpdate = new ProgressUpdater(progress);
              newSpace = copySpaceManager.copySpace(originalSpace, newKey, newName, user, options, progressUpdate);
              progress.setStatus("Task execution completed.");
              progress.setPercentage(100);
              progress.setCompletedSuccessfully(true);
              return newSpace;
          } catch (Throwable e) {
                  log.error("Failed to copy the space: " + originalSpace , e);
                  progress.setStatus("There was an error in the content creation. Please check your log files. :" + e.getMessage());
                  progress.setCompletedSuccessfully(false);
                  throw new RuntimeException("error executing task", e);
          }
          // still required? we will not forward or something like this
//          setSpace(newSpace);
//          setKey(newKey);
        }
    }

}
