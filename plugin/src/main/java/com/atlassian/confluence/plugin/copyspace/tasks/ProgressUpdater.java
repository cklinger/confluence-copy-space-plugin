package com.atlassian.confluence.plugin.copyspace.tasks;

import com.atlassian.core.util.ProgressMeter;

public class ProgressUpdater {

    private final ProgressMeter progressMeter;
    private int totalPages;

    ProgressUpdater(ProgressMeter progressMeter) {
        this.progressMeter = progressMeter;
        this.totalPages = 0;
    }

    public synchronized void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public synchronized void setCurrentPage(int currentPage) {
        this.progressMeter.setPercentage(currentPage, totalPages);
    }

    public synchronized void setMessage(String currentOperation) {
        this.progressMeter.setStatus(currentOperation);
    }

}
