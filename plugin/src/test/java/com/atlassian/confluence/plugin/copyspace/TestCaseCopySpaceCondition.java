package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

public class TestCaseCopySpaceCondition extends TestCase {
    private CopySpaceCondition copySpaceCondition;

    @Mock
    private WebInterfaceContext webInterfaceContext;
    @Mock
    private PermissionManager permissionManager;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        copySpaceCondition = new CopySpaceCondition();
        copySpaceCondition.setPermissionManager(permissionManager);
    }

    public void testWhenUserCannotCreateSpaceButCanAdministerSpace() {
        Space space = new Space();

        User user = new DefaultUser();

        boolean canCreateSpace = false;
        boolean canAdministerSpace = true;

        when(webInterfaceContext.getSpace()).thenReturn(space);
        when(webInterfaceContext.getUser()).thenReturn(user);

        when(permissionManager.hasCreatePermission(user, PermissionManager.TARGET_APPLICATION, Space.class)).thenReturn(canCreateSpace);
        when(permissionManager.hasPermission(user, Permission.ADMINISTER, space)).thenReturn(canAdministerSpace);

        assertFalse(copySpaceCondition.shouldDisplay(webInterfaceContext));
    }
}
