package com.atlassian.confluence.plugin.copyspace.actions;

import com.atlassian.confluence.plugin.copyspace.CopySpaceManager;
import com.atlassian.confluence.plugin.copyspace.CopySpaceOptions;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.user.impl.DefaultUser;
import com.opensymphony.xwork.Action;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCaseDoCopySpaceAction extends TestCase {
    private DoCopySpaceAction doCopySpaceAction;

    private Space originalSpace;
    private String newKey = "NEW";
    private String newName = "New Space";
    private ConfluenceUser user;

    @Mock
    private CopySpaceManager copySpaceManager;
    @Mock
    private PermissionManager permissionManager;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        originalSpace = new Space();
        user = new ConfluenceUserImpl(new DefaultUser("bob"));

        AuthenticatedUserThreadLocal.set(user);

        doCopySpaceAction = new DoCopySpaceAction();
        doCopySpaceAction.setCopySpaceManager(copySpaceManager);
        doCopySpaceAction.setSpace(originalSpace);
        doCopySpaceAction.setNewKey(newKey);
        doCopySpaceAction.setNewName(newName);
    }

    public void testSpaceCopiedUnderNewKey() throws Exception {
        Space newSpace = new Space();
        newSpace.setKey(newKey);
        newSpace.setName(newName);

        doCopySpaceAction.setSpace(originalSpace);
        doCopySpaceAction.setNewKey(newKey);
        doCopySpaceAction.setNewName(newName);

        assertEquals(Action.SUCCESS, doCopySpaceAction.execute());
        assertEquals(newKey, doCopySpaceAction.getKey());

        verify(copySpaceManager, atLeastOnce()).copySpace(eq(originalSpace), eq(newKey), eq(newName), eq(user), (CopySpaceOptions) anyObject());
    }

    public void testAdminPrivilegeRequiredToCopySpace() {
        boolean canCreateSpaces = true;
        boolean canAdministerThisSpace = false;

        doCopySpaceAction.setPermissionManager(permissionManager);

        when(permissionManager.hasCreatePermission(user, PermissionManager.TARGET_APPLICATION, Space.class)).thenReturn(canCreateSpaces);
        when(permissionManager.hasPermission(user, Permission.ADMINISTER, originalSpace)).thenReturn(canAdministerThisSpace);

        assertFalse(doCopySpaceAction.isPermitted());
    }
}
