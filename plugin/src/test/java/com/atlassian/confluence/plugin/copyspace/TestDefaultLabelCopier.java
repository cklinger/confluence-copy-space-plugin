package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.user.impl.DefaultUser;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import junit.framework.TestCase;

/**
 * Tests the methods in the DefaultLabelCopier using a Mock LabelManager.
 */
public class TestDefaultLabelCopier extends TestCase {
    private int idCounter = 1;
    private Mock mockLabelManager;
    private DefaultLabelCopier labelCopier;


    protected void setUp() throws Exception {
        mockLabelManager = new Mock(LabelManager.class);
        labelCopier = new DefaultLabelCopier();

//        mockLabelManager.expect("toString");
//        System.out.println("mockLabelManager.proxy() = " + mockLabelManager.proxy());

        labelCopier.setLabelManager((LabelManager) mockLabelManager.proxy());
    }

    protected void tearDown() throws Exception {
        mockLabelManager = null;
        labelCopier = null;
    }

    public void testCopyingFromContentWithNoLabelList() {
        final ContentEntityObject originalUnlabelledContent = new Page();
        final ContentEntityObject destinationPage = new Page();

        labelCopier.copyLabels(originalUnlabelledContent, destinationPage, false);
        mockLabelManager.verify();
        assertEquals(0, destinationPage.getLabelCount());

        labelCopier.copyLabels(originalUnlabelledContent, destinationPage, true);
        mockLabelManager.verify();
        assertEquals(0, destinationPage.getLabelCount());
    }

    public void testCopyingFromContentWithGlobalLabels() {
        final ContentEntityObject labelledContent = createPage();
        final ContentEntityObject destinationPage = createPage();
        Label label1 = createTestLabelOnContent(labelledContent, "foo1", Namespace.GLOBAL);
        Label label2 = createTestLabelOnContent(labelledContent, "foo2", Namespace.GLOBAL);
        Label label3 = createTestLabelOnContent(labelledContent, "foo3", Namespace.GLOBAL);

        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label1)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label2)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label3)), 1);

        labelCopier.copyLabels(labelledContent, destinationPage, false);

        mockLabelManager.verify();
        // since we are mocking the mockLabelManager no labels should get added
        assertEquals(0, destinationPage.getLabelCount());
    }

    public void testCopyingFromContentWithPersonalLabelsAllowed() {
        final ContentEntityObject labelledContent = createPage();
        final ContentEntityObject destinationPage = createPage();
        Label label1 = createTestLabelOnContent(labelledContent, "foo1", Namespace.GLOBAL);
        Label label2 = createTestLabelOnContent(labelledContent, "foo2", Namespace.PERSONAL);
        Label label3 = createTestLabelOnContent(labelledContent, "foo3", Namespace.TEAM);

        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label1)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label2)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label3)), 1);

        labelCopier.copyLabels(labelledContent, destinationPage, true);

        mockLabelManager.verify();
        // since we are mocking the mockLabelManager no labels should get added
        assertEquals(0, destinationPage.getLabelCount());
    }

    public void testCopyingFromContentWithPersonalLabelsDisAllowed() {
        final ContentEntityObject labelledContent = createPage();
        final ContentEntityObject destinationPage = createPage();
        Label label1 = createTestLabelOnContent(labelledContent, "foo1", Namespace.GLOBAL);
        Label label2 = createTestLabelOnContent(labelledContent, "foo2", Namespace.PERSONAL);
        Label label3 = createTestLabelOnContent(labelledContent, "foo3", Namespace.TEAM);

        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label1)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationPage), C.eq(label3)), 1);

        labelCopier.copyLabels(labelledContent, destinationPage, false);

        mockLabelManager.verify();
        assertEquals(0, destinationPage.getLabelCount());
    }

    public void testCopyingSpacesWithNoLabelList() {
        final Space originalUnlabelledSpace = createSpace("FROM");
        final Space destinationSpace = createSpace("TO");

        labelCopier.copySpaceLabels(originalUnlabelledSpace, destinationSpace, false);
        mockLabelManager.verify();
        assertEquals(0, destinationSpace.getDescription().getLabelCount());

        labelCopier.copySpaceLabels(originalUnlabelledSpace, destinationSpace, true);
        mockLabelManager.verify();
        assertEquals(0, destinationSpace.getDescription().getLabelCount());
    }

    public void testCopyingSpacesWithPersonalLabelsAllowed() {
        final Space originalSpace = createSpace("FROM");
        final Space destinationSpace = createSpace("TO");

        Label label1 = createTestLabelOnContent(originalSpace.getDescription(), "foo1", Namespace.GLOBAL);
        Label label2 = createTestLabelOnContent(originalSpace.getDescription(), "foo2", Namespace.PERSONAL);
        Label label3 = createTestLabelOnContent(originalSpace.getDescription(), "foo3", Namespace.TEAM);

        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationSpace.getDescription()), C.eq(label1)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationSpace.getDescription()), C.eq(label2)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationSpace.getDescription()), C.eq(label3)), 1);

        labelCopier.copySpaceLabels(originalSpace, destinationSpace, true);

        mockLabelManager.verify();
        // since we are mocking the mockLabelManager no labels should get added
        assertEquals(0, destinationSpace.getDescription().getLabelCount());
    }


    public void testCopyingSpacesWithPersonalLabelsDisAllowed() {
        final Space originalSpace = createSpace("FROM");
        final Space destinationSpace = createSpace("TO");

        Label label1 = createTestLabelOnContent(originalSpace.getDescription(), "foo1", Namespace.GLOBAL);
        Label label2 = createTestLabelOnContent(originalSpace.getDescription(), "foo2", Namespace.PERSONAL);
        Label label3 = createTestLabelOnContent(originalSpace.getDescription(), "foo3", Namespace.TEAM);

        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationSpace.getDescription()), C.eq(label1)), 1);
        mockLabelManager.expectAndReturn("addLabel", C.args(C.eq(destinationSpace.getDescription()), C.eq(label3)), 1);

        labelCopier.copySpaceLabels(originalSpace, destinationSpace, false);

        mockLabelManager.verify();
        // since we are mocking the mockLabelManager no labels should get added
        assertEquals(0, destinationSpace.getDescription().getLabelCount());
    }

    private Space createSpace(String key) {
        Space space = new Space(key);
        space.setDescription(new SpaceDescription(space));
        space.getDescription().setId(idCounter++);
        space.setId(idCounter++);
        return space;
    }


    private ContentEntityObject createPage() {
        final ContentEntityObject labelledContent = new Page();
        labelledContent.setId(idCounter++); // id is set to make the object look persistent
        labelledContent.setTitle("page " + (idCounter++));
        return labelledContent;
    }

    private Label createTestLabelOnContent(ContentEntityObject labelledContent, String labelText, Namespace namespace) {
        Label label = new Label(labelText, namespace);
        label.setId(idCounter++); // id is set to make the object look persistent
        labelledContent.addLabelling(new Labelling(label, labelledContent, new ConfluenceUserImpl(new DefaultUser("bob"))));
        return label;
    }

}
