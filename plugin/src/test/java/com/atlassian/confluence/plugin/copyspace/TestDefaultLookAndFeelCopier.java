package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.setup.settings.SpaceSettings;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.themes.ColourSchemeManager;
import com.atlassian.confluence.themes.ThemeManager;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import junit.framework.TestCase;

/**
 * Test the look and feel copier works correctly.
 */
public class TestDefaultLookAndFeelCopier extends TestCase {
    private Mock mockThemeManager = new Mock(ThemeManager.class);
    private Mock mockSettingsManager = new Mock(SettingsManager.class);
    private Mock mockColourSchemeManager = new Mock(ColourSchemeManager.class);
    private DefaultLookAndFeelCopier lookAndFeelCopier;


    protected void setUp() throws Exception {
        lookAndFeelCopier = new DefaultLookAndFeelCopier();
        lookAndFeelCopier.setThemeManager((ThemeManager) mockThemeManager.proxy());
        lookAndFeelCopier.setSettingsManager((SettingsManager) mockSettingsManager.proxy());
        lookAndFeelCopier.setColourSchemeManager((ColourSchemeManager) mockColourSchemeManager.proxy());
    }


    protected void tearDown() throws Exception {
        lookAndFeelCopier = null;
    }

    public void testCopyLookAndFeelWithNoThemesSet() {
        Space source = createSpace("SRC");
        Space destination = createSpace("DST");
        mockThemeManager.expectAndReturn("getSpaceThemeKey", C.eq("SRC"), null);
        mockThemeManager.expect("setSpaceTheme", C.args(C.eq("DST"), C.IS_NULL));

        mockSettingsManager.expectAndReturn("getSpaceSettings", C.eq(source.getKey()), new SpaceSettings("SRC"));
        mockSettingsManager.expect("updateSpaceSettings", C.IS_NOT_NULL);  // C.eq(new SpaceSettings("DST") but equals doesn't work

        mockColourSchemeManager.expectAndReturn("getColourSchemeSetting", C.eq(source), null);
        mockColourSchemeManager.expect("setColourSchemeSetting", C.args(C.eq(destination), C.IS_NULL));
        mockColourSchemeManager.expectAndReturn("getSpaceColourSchemeIsolated", C.eq(source.getKey()), null);
        mockColourSchemeManager.expect("saveSpaceColourScheme", C.args(C.eq(destination), C.IS_NULL));

        lookAndFeelCopier.copyLookAndFeel(source, destination);

        mockThemeManager.verify();
        mockSettingsManager.verify();
        mockColourSchemeManager.verify();
    }

    private Space createSpace(String key) {
        Space space = new Space(key);
        space.setDescription(new SpaceDescription(space));
        return space;
    }
}
