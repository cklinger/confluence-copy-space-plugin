package it.webdriver;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.page.content.ViewPage;
import com.atlassian.confluence.webdriver.AbstractInjectableWebDriverTest;
import it.webdriver.pageobjects.CopySpacePage;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.List;

import static com.atlassian.confluence.it.SpacePermission.ADMINISTER;
import static com.atlassian.confluence.it.SpacePermission.VIEW;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CopySpaceAcceptanceTest extends AbstractInjectableWebDriverTest {
    @Rule
    public TestName testName = new TestName();

    @Test
    public void copySpaceAndVerifyCopy() {
        final Space originalSpace = rpc.createSpace(new Space("ORIG", "Original Space"));
        rpc.grantPermissions(originalSpace, User.TEST, ADMINISTER, VIEW);
        final List<Page> originalPages = rpc.createPageHierarchy(originalSpace, 3, testName.getMethodName());

        final CopySpacePage copySpacePage = product.login(User.TEST, CopySpacePage.class, originalSpace);
        copySpacePage.setCopyKey("NEW").setCopyName("Copy of Space").submit();

        final Space newSpace = rpc.getSpace("NEW");
        assertNotNull("New copied space should exist", newSpace);

        final ViewPage newSpaceHomePage = product.visit(ViewPage.class, newSpace.getHomePage().getIdAsString());

        assertEquals("Space key of new space home page should be correct", "NEW", newSpaceHomePage.getSpaceKey());
        assertEquals("New space homepage title should be same as original space homepage title", "Original Space Home", newSpaceHomePage.getTitle());

        for (Page originalPage : originalPages) {
            final Page newPage = rpc.getExistingPage(newSpace, originalPage.getTitle());
            assertNotNull("New space should contain a copied page with title: '" + originalPage.getTitle() + "'", newPage);
            final ViewPage viewNewPage = product.visit(ViewPage.class, newPage.getIdAsString());
            assertEquals("Space key of new page should be correct", "NEW", viewNewPage.getSpaceKey());
            assertEquals("New space page title should be same as original space page title", originalPage.getTitle(), viewNewPage.getTitle());
            waitUntilEquals("New space page content should be same as original space page content", originalPage.getContent(), viewNewPage.getMainContent().timed().getText());
        }

    }
}
